<link href="magiczoomplus/magiczoomplus.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="css/tab.css" type="text/css" rel="stylesheet" />
<div class="breadcrumb">
    <div class="container"><?=$bread->display();?></div>
</div>
<h1 hidden="true"><?=$title_cat?></h1>
<div class="cach_top">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12 col-ttzz">
                <h2 class="why-tit">Thông tin chi tiết</h2>
                <div class="box_container">
                    <div class="wap_pro clearfix">
                        <div class="zoom_slick">
                            <div class="slick2">
                                <a data-zoom-id="Zoom-detail" id="Zoom-detail" class="MagicZoom" href="<?php if($row_detail['photo'] != NULL)echo _upload_sanpham_l.$row_detail['photo'];else echo 'images/noimage.gif';?>" title="<?=$row_detail['ten']?>"><img class='cloudzoom' src="<?php if($row_detail['photo'] != NULL)echo _upload_sanpham_l.$row_detail['photo'];else echo 'images/noimage.gif';?>" /></a>
                                <?php $count=count($hinhthem); if($count>0) {?>
                                <?php for($j=0,$count_hinhthem=count($hinhthem);$j<$count_hinhthem;$j++){?>
                                    <a data-zoom-id="Zoom-detail" id="Zoom-detail" class="MagicZoom" href="<?php if($hinhthem[$j]['photo']!=NULL) echo _upload_hinhthem_l.$hinhthem[$j]['photo']; else echo 'images/noimage.gif';?>" title="<?=$row_detail['ten']?>" ><img src="<?php if($hinhthem[$j]['photo']!=NULL) echo _upload_hinhthem_l.$hinhthem[$j]['photo']; else echo 'images/noimage.gif';?>" /></a>
                                <?php }} ?>
                            </div>
                            <?php $count=count($hinhthem); if($count>0) {?>
                            <div class="slick">
                                <p><img src="thumb/100x80x2x90/<?php if($row_detail['photo'] != NULL)echo _upload_sanpham_l.$row_detail['photo'];else echo 'images/noimage.gif';?>" /></p>
                                <?php for($j=0,$count_hinhthem=count($hinhthem);$j<$count_hinhthem;$j++){?>
                                    <p><img src="thumb/100x80x2x90/<?php if($hinhthem[$j]['thumb']!=NULL) echo _upload_hinhthem_l.$hinhthem[$j]['thumb']; else echo 'images/noimage.gif';?>" /></p>
                                <?php } ?>
                            </div>
                            <?php } ?>
                        </div>
                    
                        <ul class="product_info">
                            <li class="ten"><?=$row_detail['ten']?></li>
                            <?php if($row_detail['masp'] != '') { ?><li><b><?=_masanpham?>:</b> <?=$row_detail['masp']?></span></li><?php } ?>
                            <li class="gia"><b><?=_gia?>:</b> <?php if($row_detail['gia'] != 0)echo number_format($row_detail['gia'],0, ',', '.').' <sup>đ</sup>';else echo 'Liên hệ'; ?></li>
                            <li><b><?=_luotxem?>:</b> <span><?=$row_detail['luotxem']?></span></li>
                            <?php if($row_detail['mota'] != '') { ?><li><?=nl2br($row_detail['mota'])?></li><?php } ?>
                            <li><div class="addthis_native_toolbox"></div></li>
                        </ul>
                    </div>
                    <div id="tabs">
                        <ul id="ultabs">
                            <li data-vitri="0"><?=_thongtinsanpham?></li>
                        </ul>
                        <div style="clear:both"></div>

                        <div id="content_tabs">
                            <div class="tab">
                                <?=$row_detail['noidung']?>
                            </div>
                            <div class="fb-comments" data-href="<?=getCurrentPageURL()?>" data-numposts="5" data-width="100%"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12 col-rrr">
                <?php include _template."layout/right_t.php";?>
            </div>
        </div>
    </div>
</div>
