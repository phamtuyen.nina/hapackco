<div class="breadcrumb">
    <div class="container"><?=$bread->display();?></div>
</div>
<h1 hidden="true"><?=$title_cat?></h1>
<div class="cach_top">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12 col-ttzz">
                <h2 class="why-tit"><?=$title_cat?></h2>
                <?php if($noidung_t!=''){ ?>
                    <div class="nn_mta"><?=$noidung_t?></div>
                <?php }?>
                <div class="row1 row_sp">
                    <?php foreach ($product as $v) {?>
                    <div class="col-md-4 col-sm-6 col-xs-6 col-pr">
                        <div class="pad_product">
                            <a href="hop-giay/<?=$v['tenkhongdau']?>.html">
                                <img src="thumb/280x280x2x90/<?=_upload_sanpham_l.$v['photo']?>" alt="<?=$v['ten']?>">
                                <p>Xem thêm</p>
                            </a>
                            <div class="hpro-content">
                                <h3><a href="hop-giay/<?=$v['tenkhongdau']?>.html"><?=$v['ten']?></a></h3>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                </div>
                <div class="clear"></div>
                <div class="pagination"><?=pagesListLimitadmin($url_link , $totalRows , $pageSize, $offset)?></div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12 col-rrr">
                <?php include _template."layout/right_t.php";?>
            </div>
        </div>
    </div>
</div>
