<?php 
	$d->reset();
	$sql = "select noidung$lang as noidung,photo from #_about where type='mmabout' limit 0,1";
	$d->query($sql);
	$mm_about = $d->fetch_array();

	$d->reset();
	$sql = "select noidung$lang as noidung,video from #_about where type='vdabout' limit 0,1";
	$d->query($sql);
	$vd_about = $d->fetch_array();

	$d->reset();
    $sql = "select ten$lang as ten,link,photo,mota$lang as mota from #_slider where hienthi=1 and type='icon-tam-nhin' order by stt,id desc";
    $d->query($sql);
    $tamnhin=$d->result_array(); 	

	$d->reset();
	$sql = "select id,ten$lang as ten,tenkhongdau,photo from #_news where type='y-kien-khach-hang' and hienthi=1 order by stt,id desc";
	$d->query($sql);
	$ykien = $d->result_array();

    $d->reset();
    $sql_product_danhmuc="select ten$lang as ten,tenkhongdau,id,mota$lang as mota,photo from #_product_danhmuc where hienthi=1 and type='hop-giay' order by stt,id desc limit 0,4";
    $d->query($sql_product_danhmuc);
    $product_danhmuc1=$d->result_array(); 	

    $d->reset();
    $sql = "select ten$lang as ten,link,photo,mota$lang as mota from #_slider where hienthi=1 and type='tai-sao-chon-chung-toi' order by stt,id desc";
    $d->query($sql);
    $taisao=$d->result_array(); 

 	$d->reset();
    $sql = "select photo,link from #_background where type='taisao' limit 0,1";
    $d->query($sql);
    $row_hinhanh = $d->fetch_array();  

     $d->reset();
    $sql = "select id,ten$lang as ten,tenkhongdau,photo,noidung$lang as noidung,mota$lang as mota from #_news where type='y-kien-khach-hang' and hienthi=1 order by stt,id desc";
    $d->query($sql);
    $ykien = $d->result_array();                
?>

<div class="ab_banner lazy" style="background: linear-gradient(rgba(168,102,51,.2), rgba(168,102,51,.2)), url(<?=_upload_hinhanh_l.$mm_about['photo']?>)">
	<div class="container">
		<div class="banner-content">
			<h1>Giới thiệu</h1>
			<div class="banner-content-wrap">
				<?=$mm_about['noidung']?>
			</div>
		</div>
	</div>
</div>
<div class="about-page">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h2 class="why-tit">Giới thiệu về chúng tôi</h2>
				<div class="aboutp-content"><?=$tintuc_detail['noidung']?></div>
			</div>
			<div class="col-md-6">
				<h2 class="tt_sp">Sản phẩm nổi bật</h2>
				<div class="aboutp-row">
					<?php foreach ($product_danhmuc1 as $v) {?>
					<div class="col-xs-6">
						<div class="aboutp-item">
							<a href="san-pham/<?=$v['tenkhongdau']?>" title="<?=$v['ten']?>"><img src="thumb/450x450x1x90/<?=_upload_sanpham_l.$v['photo']?>" title="<?=$v['ten']?>" alt="<?=$v['ten']?>" class="w-100">
							<h3><?=$v['ten']?></h3>
							</a>
							
						</div>
					</div>
					<?php }?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="about-page">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h2 class="why-tit">Video giới thiệu</h2>
				<div class="aboutp-content"><?=$vd_about['noidung']?></div>
				<div class="text-lg-left text-center">
					<a href="<?=$company['youtube']?>" class="td_video" title="">Theo dõi chúng tôi trên youtube</a>
				</div>
			</div>
			<div class="col-md-6">
				<div class="video_popup left_video">
					<iframe title="Video giới thiệu" width="100%" src="http://www.youtube.com/embed/<?php preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $vd_about['video'], $matches);echo $matches[1];?>" frameborder="0" allowfullscreen></iframe></div>
			</div>
		</div>
	</div>
</div>

<div class="aboutp-mision">
	<div class="container">
		<div class="row justify-content-center">
			<?php foreach ($tamnhin as $v) {?>
				<div class="col-lg-4 col-md-3">
			<div class="amision-item">
					<h2 class="d-flex align-items-center bold italic s24 t1 amision-tit">
						<img class="mr-3" src="<?=_upload_hinhanh_l.$v['photo']?>" title="<?=$v['ten']?>" alt="<?=$v['ten']?>">
						<?=$v['ten']?></h2>
					<div class="amision-content">
						<p><?=$v['mota']?></p>
					</div>
			</div>
			</div>
			<?php }?>
		</div>
	</div>
</div>

<div class="ahotline lazy" data-src="images/body.jpg">
	<div class="container text-center text-white">
		<h2 >Bạn cần giải đáp hoặc tư vấn</h2>
		<p class="f1py-2">Hotline: <a title="" href="tel:<?=$company['dienthoai']?>"><?=$company['dienthoai']?></a></p>
		<p class="f1py-3">Vui lòng liên hệ đội ngũ kỹ thuật viên của chúng tôi. Chúng tôi luôn hỗ trợ nhiệt tình 24/7.</p>
		<div class="pt-4text-center">
			<a href="lien-he.html">Liên hệ ngay</a>
		</div>
	</div>
</div>


<div class="why">
    <div class="container">
        <h2 class="why-tit">Tại sao chọn chúng tôi</h2>
        <div class="row">
            <div class="col-sm-6">
                <?php foreach ($taisao as $key => $v) {?>
                <div class="why-item wow fadeInUp" data-wow-offset="150">
                    <div class="row">
                        <div class="col-xs-3 text-center">
                            <img src="<?=_upload_hinhanh_l.$v['photo']?>" title="<?=$v['ten']?>" alt="<?=$v['ten']?>">
                        </div>
                        <div class="col-xs-9 d-flex align-items-center">
                            <div class="s14 why-content">
                                <h3 class="why-text-uppercase"><?=$v['ten']?></h3>
                                <p><?=nl2br($v['mota'])?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }?>
            </div>
            <div class="col-sm-6 wow bounceInDown" data-wow-offset="150"  data-wow-delay=".6s">
                    <img src="<?=_upload_hinhanh_l.$row_hinhanh['photo']?>" title="Tại sao chọn chúng tôi" alt="Tại sao chọn chúng tôi">
            </div>
        </div>
    </div>
</div>

<div class="ykienkkk lazy" data-src="images/body.jpg">
	<div class="container">
		 <h2 class="text-white-exbold-product">Ý kiến khách hàng</h2>
		 <div class="skicl_ykien">
		 	<?php foreach ($ykien as $v) {?>
			<div>
				<div class="tes-item">
					<div class="tes-item-content">
						<?=$v['noidung']?>
					</div>
					<img src="thumb/100x100x1x90/<?=_upload_tintuc_l.$v['photo']?>" title="<?=$v['ten']?>" alt="<?=$v['ten']?>">
					<p class="bold-s24"><?=$v['ten']?></p>
					<p class="s16-mm"><?=$v['mota']?></p>
				</div>
			</div>
		 	<?php }?>
		 </div>
	</div>
</div>

