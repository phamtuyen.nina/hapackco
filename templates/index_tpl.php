<?php 
    $d->reset();
    $sql = "select mota$lang as mota,ten$lang as ten from #_about where type='about' limit 0,1";
    $d->query($sql);
    $company_about = $d->fetch_array();

    $d->reset();
    $sql = "select ten$lang as ten,link,photo,mota$lang as mota from #_slider where hienthi=1 and type='icon-gioi-thieu' order by stt,id desc";
    $d->query($sql);
    $icongt=$d->result_array();  

    $d->reset();
    $sql = "select ten$lang as ten,link,photo,mota$lang as mota from #_slider where hienthi=1 and type='tai-sao-chon-chung-toi' order by stt,id desc";
    $d->query($sql);
    $taisao=$d->result_array(); 

    $d->reset();
    $sql = "select ten$lang as ten,link,photo,mota$lang as mota from #_slider where hienthi=1 and type='uu-diem-noi-bat' order by stt,id desc";
    $d->query($sql);
    $udiem=$d->result_array();      

    $d->reset();
    $sql = "select photo,link from #_background where type='taisao' limit 0,1";
    $d->query($sql);
    $row_hinhanh = $d->fetch_array(); 

    $d->reset();
    $sql = "select id,ten$lang as ten,tenkhongdau,photo,ngaytao,mota$lang as mota from #_news where type='tin-tuc' and hienthi=1 and noibat=1 order by stt,id desc";
    $d->query($sql);
    $tinmoi = $d->result_array(); 

    $d->reset();
    $sql_product_danhmuc="select ten$lang as ten,tenkhongdau,id,mota$lang as mota,photo from #_product_danhmuc where hienthi=1 and type='hop-giay' order by stt,id desc";
    $d->query($sql_product_danhmuc);
    $product_danhmuc=$d->result_array();   

     $d->reset();
    $sql = "select id,ten$lang as ten,tenkhongdau,icon,mota$lang as mota from #_news where type='ung-dung' and hienthi=1 order by stt,id desc";
    $d->query($sql);
    $ungdung = $d->result_array();   

     $d->reset();
    $sql = "select id,ten$lang as ten,tenkhongdau,photo from #_product where type='hop-giay' and hienthi=1 and noibat=1 order by stt,id desc";
    $d->query($sql);
    $product = $d->result_array();   

?>
<h1 hidden="true"><?=$title_cat?></h1>
<div class="pt-4 babout">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h2 class="exbold"><?=$company_about['ten']?></h2>
                <div class="babout-wrap">
                    <?=$company_about['mota']?>
                </div>
                <div class="text-sm-left text-center pt-3">
                    <a href="gioi-thieu.html" title="" class=" more-btn">Xem thêm </a>
                </div>
            </div>
            <div class="col-sm-6">
                <?php foreach ($icongt as $k => $v) {?>
                <div class="babout-item wow fadeInUp" data-wow-offset="150">
                    <h3 class="exbold s14 t1 f1 text-uppercase about-item-tit"><a href="<?=$v['link']?>" title="<?=$v['ten']?>"><?=$v['ten']?></a></h3>
                    <div class="row">
                        <div class="col-lg-2 col-xs-2 col-4">
                            <div class="babout-img">
                                <a href="<?=$v['link']?>" title="<?=$v['ten']?>"><img src="thumb/80x60x1x90/<?=_upload_hinhanh_l.$v['photo']?>" title="<?=$v['ten']?>" alt="<?=$v['ten']?>"></a>
                            </div>
                        </div>
                        <div class="col-lg-10 col-8 col-xs-10 d-flex align-items-center">
                            <div class="s14 babout-content">
                                <p><?=$v['mota']?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
    </div>
</div>
<div class="wap_dmpr">
<div class="slick_dm_sp">
    <?php foreach ($product_danhmuc as $key => $v) {?>
    <div class="col-xs1">
        <div class="pad_dm">
            <a href="hop-giay/<?=$v['tenkhongdau']?>">
                <img src="thumb/450x450x1x90/<?=_upload_sanpham_l.$v['photo']?>" alt="<?=$v['ten']?>">
                <div class="hover_nd">
                    <h2><?=$v['ten']?></h2>
                    <p><?=catchuoi(trim(strip_tags($v['mota'])),200)?></p>
                </div>
            </a>
        </div>
    </div>
    <?php }?>
</div>
</div>
<div class="why">
    <div class="container">
        <h2 class="why-tit">Tại sao chọn chúng tôi</h2>
        <div class="row">
            <div class="col-sm-6">
                <?php foreach ($taisao as $key => $v) {?>
                <div class="why-item wow fadeInUp" data-wow-offset="150">
                    <div class="row">
                        <div class="col-xs-3 text-center">
                            <img src="<?=_upload_hinhanh_l.$v['photo']?>" title="<?=$v['ten']?>" alt="<?=$v['ten']?>">
                        </div>
                        <div class="col-xs-9 d-flex align-items-center">
                            <div class="s14 why-content">
                                <h3 class="why-text-uppercase"><?=$v['ten']?></h3>
                                <p><?=nl2br($v['mota'])?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }?>
            </div>
            <div class="col-sm-6 wow bounceInDown" data-wow-offset="150"  data-wow-delay=".6s">
                    <img src="<?=_upload_hinhanh_l.$row_hinhanh['photo']?>" title="Tại sao chọn chúng tôi" alt="Tại sao chọn chúng tôi">
            </div>
        </div>
    </div>
</div>
<div class="wap_spmoi lazy" data-src="images/body.jpg">
    <div class="container">
        <h2 class="text-white-exbold-product">Sản phẩm nổi bật</h2>
        <div class="row">
            <div class="slick_product">
                <?php foreach ($product as $key => $v) {?>
                <div class="col-xs-3">
                    <div class="pad_product">
                        <a href="hop-giay/<?=$v['tenkhongdau']?>.html">
                            <img src="thumb/280x280x2x90/<?=_upload_sanpham_l.$v['photo']?>" alt="<?=$v['ten']?>">
                            <p>Xem thêm</p>
                        </a>
                        <div class="hpro-content">
                            <h3><a href="hop-giay/<?=$v['tenkhongdau']?>.html"><?=$v['ten']?></a></h3>
                        </div>
                    </div>
                </div>
               
                <?php }?>
            </div>
        </div>
    </div>
</div>



<div class="wap_uudiem">
     <div class="container">
        <h2 class="text-white-exbold-product text-white-exbold-uudiem">Ưu điểm nổi bật</h2>
        <div class="ud-wrap">
            <div class="row justify-content-around">
                <?php foreach ($udiem as $k => $v) {?>
                <div class="col-lg-2 col-md-4 col-sm-4 col-6">
                    <div class="text-center ud-item wow rollIn" data-wow-offset="150">
                        <a href="<?=$v['link']?>" title="<?=$v['ten']?>"><img src="<?=_upload_hinhanh_l.$v['photo']?>" title="<?=$v['ten']?>" alt="<?=$v['ten']?>"></a>
                        <h3><a href="<?=$v['link']?>" title="<?=$v['ten']?>"><?=$v['ten']?></a></h3>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
    </div>
</div>

<div class="wap_uudiem wap_ungdung">
    <div class="container">
        <h2 class="text-white-exbold-product text-white-exbold-uudiem">Ứng dụng của sản phẩm</h2>
        <div class="row justify-content-center">
            <?php foreach ($ungdung as $key => $v) {?>
            <div class="col-lg-4 col-md-6">
                <div class="d-flex align-items-start app-item wow fadeInUp" data-wow-offset="150">
                    <img src="<?=_upload_tintuc_l.$v['icon']?>" alt="<?=$v['ten']?>">
                    <div class="app-item-content">
                        <h3><a href="ung-dung/<?=$v['tenkhongdau']?>.html" title="<?=$v['ten']?>"><?=$v['ten']?></a></h3>
                        <p><?=$v['mota']?></p>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
    </div>
</div>

<div class="wap_tintuc lazy" data-src="images/body.jpg">
    <div class="container">
        <h2 class="text-white-exbold-product">Tin tức nổi bật</h2>
        <div class="row">
            <div class="slick_tintuc">
                <?php foreach ($tinmoi as $key => $v) {?>
                <div class="col-xs-4">
                    <div class="pad_tin">
                        <a href="tin-tuc/<?=$v['tenkhongdau']?>.html">
                            <img onError="this.src='http://placehold.it/380x250';" src="thumb/380x250x1x90/<?=_upload_tintuc_l.$v['photo']?>" alt="<?=$v['ten']?>">
                        </a>
                        <div class="info_tin">
                           <h3>
                               <a href="tin-tuc/<?=$v['tenkhongdau']?>.html"><?=$v['ten']?></a>
                           </h3>
                           <p><i class="fa fa-calendar" aria-hidden="true"></i> <?=date('d/m/Y',$v['ngaytao'])?></p>
                           <div>
                               <?=catchuoi(trim(strip_tags($v['mota'])),200)?>
                           </div>
                        </div>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
    </div>
</div>


