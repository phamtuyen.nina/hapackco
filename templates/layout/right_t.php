<?php
	$d->reset();
	$sql_product_danhmuc="select ten$lang as ten,tenkhongdau,id from #_product_danhmuc where hienthi=1 and type='hop-giay' order by stt,id desc";
	$d->query($sql_product_danhmuc);
	$product_danhmuc=$d->result_array();

	$d->reset();
	$sql = "select id,ten$lang as ten,tenkhongdau,photo,mota$lang as mota from #_news where type='tin-tuc' and hienthi=1 order by ngaytao desc limit 0,4";
	$d->query($sql);
	$tinmoi = $d->result_array();	
?>
<div class="baside">
	<div class="plist">
		<h3 class="baside-tit">Danh mục sản phẩm</h3>
		<ul>
			<?php foreach ($product_danhmuc as $v) {?>
			<li>
				<a href="hop-giay/<?=$v['tenkhongdau']?>"><?=$v['ten']?></a>
			</li>
			<?php }?>
		</ul>
	</div>
</div>

<div class="baside">
	<div class="plist"><h3 class="baside-tit">Tin tức mới</h3>
	<div class="showw">
		<?php foreach ($tinmoi as $v) {?>
		<div class="baside-item">
            <h4><a href="tin-tuc/<?=$v['tenkhongdau']?>.html" title="<?=$v['ten']?>"><?=$v['ten']?></a></h4>
            <div class="row">
              <div class="pad5"><a href="tin-tuc/<?=$v['tenkhongdau']?>.html" title="<?=$v['ten']?>"><img onError="this.src='http://placehold.it/90x90';" src="thumb/90x90x1x90/<?=_upload_tintuc_l.$v['photo']?>" class="" alt="<?=$v['ten']?>"></a></div>
              <div class="pad5"><?=catchuoi(trim(strip_tags($v['mota'])),80)?></div>
            </div>
        </div>
		<?php }?>
	</div>
	</div>
</div>