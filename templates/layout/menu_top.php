<?php
	$d->reset();
	$sql_product_danhmuc="select ten$lang as ten,tenkhongdau,id from #_product_danhmuc where hienthi=1 and type='hop-giay' order by stt,id desc";
	$d->query($sql_product_danhmuc);
	$product_danhmuc=$d->result_array();
?>

<ul>	
    <li><a class="<?php if((!isset($_REQUEST['com'])) or ($_REQUEST['com']==NULL) or $_REQUEST['com']=='index') echo 'active'; ?>" href="index.html"><?=_trangchu?></a></li>
    <li><a class="<?php if($_REQUEST['com'] == 'gioi-thieu') echo 'active'; ?>" href="gioi-thieu.html"><?=_gioithieu?></a></li>
    <li><a class="<?php if($_REQUEST['com'] == 'hop-giay') echo 'active'; ?>" href="hop-giay.html">Hộp giấy</a>
    	<ul>
			<?php for($i = 0;$i<count($product_danhmuc); $i++){ 
			
				$d->reset();
				$sql_product_list="select ten$lang as ten,tenkhongdau,id from #_product_list where hienthi=1 and id_danhmuc='".$product_danhmuc[$i]['id']."' order by stt,id desc";
				$d->query($sql_product_list);
				$product_list=$d->result_array();			
			?>
            <li><a href="hop-giay/<?=$product_danhmuc[$i]['tenkhongdau']?>"><?=$product_danhmuc[$i]['ten']?></a>
                
                <?php if(count($product_list)>0){?>
                <ul>
					
                     <?php for($j = 0;$j < count($product_list); $j++){ 
					 
					$d->reset();
					$sql_product_list="select ten$lang as ten,tenkhongdau,id from #_product_cat where hienthi=1 and id_list='".$product_list[$j]['id']."' order by stt,id desc";
					$d->query($sql_product_list);
					$product_cat=$d->result_array();
					 
					 ?>
                            <li><a href="hop-giay/<?=$product_list[$j]['tenkhongdau']?>/"><?=$product_list[$j]['ten']?></a>
                            
                            <?php if(count($product_cat)>0){?>
                            <ul>                            
								<?php for($k = 0;$k < count($product_cat); $k++){ ?>
                                    <li><a href="hop-giay/<?=$product_cat[$k]['tenkhongdau']?>.htm"><?=$product_cat[$k]['ten']?></a></li>
                                <?php } ?>
                            </ul>
                            <?php } ?>
                                
                            </li>
                     <?php } ?>
                 </ul>
                 <?php } ?>
                </li>
                <?php } ?>
            </ul>	
    </li>
    <li><a class="<?php if($_REQUEST['com'] == 'tu-van') echo 'active'; ?>" href="tu-van.html">Tư vấn</a></li>
    <li><a class="<?php if($_REQUEST['com'] == 'tin-tuc') echo 'active'; ?>" href="tin-tuc.html"><?=_tintuc?></a></li>
    <li><a class="<?php if($_REQUEST['com'] == 'lien-he') echo 'active'; ?>" href="lien-he.html"><?=_lienhe?></a></li>
</ul>
<div class="g1-drop">
    <a class="g1-drop-toggle" href="javascript:void"> <i class="fa fa-search"></i>Tìm kiếm <span class="g1-drop-toggle-arrow"></span> </a>
    <div class="v_Search">
        <div class="w_timk">
            <input type="text" name="keyword" id="keyword" onKeyPress="doEnter(event,'keyword');" value="<?=_nhaptukhoatimkiem?>..."  onclick="if(this.value=='<?=_nhaptukhoatimkiem?>...'){this.value=''}" onblur="if(this.value==''){this.value='<?=_nhaptukhoatimkiem?>...'}">
            <i class="fa fa-search" aria-hidden="true" onclick="onSearch(event,'keyword');"></i>
        </div>
    </div>
</div>
